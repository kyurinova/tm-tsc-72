package ru.tsc.kyurinova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.dto.model.AbstractEntityDTO;

import java.util.List;

public interface IDTOService<E extends AbstractEntityDTO> {

    @NotNull
    List<E> findAll();

    void clear();

}

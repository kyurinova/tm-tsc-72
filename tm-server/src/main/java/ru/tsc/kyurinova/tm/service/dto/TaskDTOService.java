package ru.tsc.kyurinova.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kyurinova.tm.enumerated.Status;

import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;
import ru.tsc.kyurinova.tm.repository.dto.TaskDTORepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class TaskDTOService extends AbstractOwnerDTOService<TaskDTO> implements ITaskDTOService {

    @NotNull
    @Autowired
    public TaskDTORepository taskRepository;

    @Override
    @Transactional
    public void addAll(@NotNull final List<TaskDTO> tasks) {
        for (TaskDTO task : tasks) {
            taskRepository.save(task);
        }
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDTO task = new TaskDTO(userId, name, "");
        taskRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskDTO task = new TaskDTO(userId, name, description);
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    public TaskDTO findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        TaskDTO taskDTO = findByName(userId, name);
        if (taskDTO != null) {
            taskRepository.deleteByUserIdAndName(userId, name);
        }
    }

    @Override
    @Transactional
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        TaskDTO taskDTO = findById(userId, id);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        taskRepository.save(taskDTO);
    }

    @Override
    @Transactional
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        TaskDTO taskDTO = findByIndex(userId, index);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        taskRepository.save(taskDTO);
    }

    @Override
    @Transactional
    public void startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        TaskDTO task = findById(userId, id);
        task.setStatus(Status.IN_PROGRESS);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        TaskDTO task = findByIndex(userId, index);
        task.setStatus(Status.IN_PROGRESS);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        TaskDTO task = findByName(userId, name);
        task.setStatus(Status.IN_PROGRESS);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        TaskDTO task = findById(userId, id);
        task.setStatus(Status.COMPLETED);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        TaskDTO task = findByIndex(userId, index);
        task.setStatus(Status.COMPLETED);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        TaskDTO task = findByName(userId, name);
        task.setStatus(Status.COMPLETED);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        TaskDTO task = findById(userId, id);
        task.setStatus(status);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        TaskDTO task = findByIndex(userId, index);
        task.setStatus(status);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        TaskDTO task = findByName(userId, name);
        task.setStatus(status);
        taskRepository.save(task);
    }

    @Nullable
    @Override
    public TaskDTO findByProjectAndTaskId(@Nullable final String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        return taskRepository.findByUserIdAndProjectIdAndId(userId, projectId, taskId);
    }

}

package ru.tsc.kyurinova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.model.AbstractModel;

import java.util.List;

public interface IService<E extends AbstractModel> {

    @NotNull
    List<E> findAll();

    void clear();

}

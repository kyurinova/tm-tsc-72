package ru.tsc.kyurinova.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.api.service.IAdminDataService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class Backup extends Thread {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final IAdminDataService adminDataService;

    @NotNull
    private static final int INTERVAL = 30;

    @NotNull
    private static final String COMMAND_SAVE = "backup-save";

    @NotNull
    private static final String COMMAND_LOAD = "backup-load";

    public Backup(Bootstrap bootstrap, @NotNull IAdminDataService adminDataService) {
        this.bootstrap = bootstrap;
        this.adminDataService = adminDataService;
        this.setDaemon(true);
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void save() {
        adminDataService.dataBackupSave();
    }

    public void load() {
        adminDataService.dataBackupLoad();
    }

    public void stopProc() {
        es.shutdown();
    }
}

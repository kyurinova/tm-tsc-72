package ru.tsc.kyurinova.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.model.IProjectTaskService;
import ru.tsc.kyurinova.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.repository.model.ProjectRepository;
import ru.tsc.kyurinova.tm.repository.model.TaskRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    public ProjectRepository projectRepository;

    @NotNull
    @Autowired
    public TaskRepository taskRepository;

    @Override
    @Transactional
    public void bindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        Project project = projectRepository.findByUserIdAndId(userId, projectId);
        Task task = taskRepository.findByUserIdAndId(userId, taskId);
        if (project == null) throw new ProjectNotFoundException();
        if (task == null) throw new TaskNotFoundException();
        task.setProject(project);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void unbindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        Project project = projectRepository.findByUserIdAndId(userId, projectId);
        Task task = taskRepository.findByUserIdAndId(userId, taskId);
        if (project == null) throw new ProjectNotFoundException();
        if (task == null) throw new TaskNotFoundException();
        if (task.getProject().getId().equals(projectId))
            task.setProject(null);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void removeAllTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.deleteAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        removeAllTaskByProjectId(userId, projectId);
        Project project = projectRepository.findByUserIdAndId(userId, projectId);
        projectRepository.delete(project);
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@Nullable final String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

}

package ru.tsc.kyurinova.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.listener.AbstractListener;

@Component
public class BackupLoadListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "backup-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load backup from XML";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@backupLoadListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        adminDataEndpoint.dataBackupLoad(sessionService.getSession());
    }

}

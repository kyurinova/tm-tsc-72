package ru.tsc.kyurinova.tm.constant;

import org.jetbrains.annotations.NotNull;

public class ProjectTestData {

    @NotNull
    public final static String USER_NAME = "test";

    @NotNull
    public final static String USER_PASSWORD = "test";

    @NotNull
    public final static String PROJECT1_NAME = "Test Project 1";

    @NotNull
    public final static String PROJECT1_DESCRIPTION = "Test Project Description 1";

    @NotNull
    public final static String PROJECT2_NAME = "Test Project 2";

    @NotNull
    public final static String PROJECT2_DESCRIPTION = "Test Project Description 2";

    @NotNull
    public final static String PROJECT3_NAME = "Test Project 3";

    @NotNull
    public final static String PROJECT3_DESCRIPTION = "Test Project Description 3";

}

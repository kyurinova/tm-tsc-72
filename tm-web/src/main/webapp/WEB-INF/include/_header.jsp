<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>TASK MANAGER</title>
</head>
<style>
    h1 {
        font-family: Trebuchet MS;
    }

    a {
        font-family: Trebuchet MS;
        color: darkblue;
    }

    select {
        font-family: Trebuchet MS;
        width: 200px;
    }

    input[type="text"] {
        font-family: Trebuchet MS;
        width: 200px;
    }

    input[type="date"] {
        font-family: Trebuchet MS;
        width: 200px;
    }

</style>

<body>
<table width="100%" height="100%" border="1" style="border-collapse: collapse">
    <tr>
        <td height="35" width="200" nowrap="nowrap" align="center">
            <b>TASK MANAGER</b>
        </td>
        <td width="100%" align="center">
            <sec:authorize access="!isAuthenticated()"><a href="/login">LOGIN</a></sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <a href="/projects">PROJECTS</a> |
                <a href="/tasks">TASKS</a> |
                USER: <sec:authentication property="name"/> |
                <a href="/logout">LOGOUT</a>
            </sec:authorize>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="100%" valign="top" style="border-collapse: collapse">
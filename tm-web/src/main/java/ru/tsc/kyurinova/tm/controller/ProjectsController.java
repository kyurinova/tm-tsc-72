package ru.tsc.kyurinova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.kyurinova.tm.api.service.model.IProjectService;
import ru.tsc.kyurinova.tm.model.CustomUser;

@Controller
public class ProjectsController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/projects")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) throws Exception {
        return new ModelAndView("project-list", "projects", projectService.findAllByUserId(user.getUserId()));
    }

}

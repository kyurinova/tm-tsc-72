package ru.tsc.kyurinova.tm.exeption;

import org.jetbrains.annotations.NotNull;

public class AbstractException extends RuntimeException {

    public AbstractException() {
        super();
    }

    public AbstractException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractException(@NotNull String s) {
        super(s);
    }

}

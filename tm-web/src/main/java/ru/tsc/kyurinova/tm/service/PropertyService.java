package ru.tsc.kyurinova.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:config.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['database.user']}")
    private String dBUser;

    @Value("#{environment['database.password']}")
    private String dBPassword;

    @Value("#{environment['database.url']}")
    private String dBUrl;

    @Value("#{environment['database.driver']}")
    private String dBDriver;

    @Value("#{environment['database.sql_dialect']}")
    private String dBSqlDialect;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String dBHbm2ddlAuto;

    @Value("#{environment['database.show_sql']}")
    private String dBShowSql;

    @Value("#{environment['database.format_sql']}")
    private String dBFormatSql;

    @Value("#{environment['database.second_lvl_cache']}")
    private String dBSecondLevelCache;

    @Value("#{environment['database.factory_class']}")
    private String dBCacheRegionFactory;

    @Value("#{environment['database.use_query_cache']}")
    private String dBUseQueryCache;

    @Value("#{environment['database.use_min_puts']}")
    private String dBUseMinPuts;

    @Value("#{environment['database.region_prefix']}")
    private String dBCacheRegionPrefix;

    @Value("#{environment['database.config_file_path']}")
    private String dBCacheProviderConfig;

}

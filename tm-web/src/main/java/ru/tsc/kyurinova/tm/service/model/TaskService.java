package ru.tsc.kyurinova.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kyurinova.tm.api.service.model.IProjectService;
import ru.tsc.kyurinova.tm.api.service.model.ITaskService;
import ru.tsc.kyurinova.tm.api.service.model.IUserService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exeption.empty.EmptyFieldException;
import ru.tsc.kyurinova.tm.exeption.empty.EmptyIdException;
import ru.tsc.kyurinova.tm.exeption.empty.EmptyUserException;
import ru.tsc.kyurinova.tm.exeption.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.repository.model.TaskRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @Autowired
    private TaskRepository repository;

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @Transactional
    public @NotNull Task add(@NotNull Task model) throws Exception {
        return repository.save(model);
    }

    @NotNull
    @Override
    @Transactional
    public Task addByUserId(@Nullable final String userId, @NotNull final Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        model.setUser(userService.findOneById(userId));
        return repository.save(model);
    }

    @Override
    @Transactional
    public void changeTaskStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyFieldException("Status");
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.save(task);
    }

    @Override
    @Transactional
    public void changeTaskStatusByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyFieldException("Status");
        @Nullable final Task task = findOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.save(task);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void clearByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        repository.deleteByUser(userService.findOneById(userId));
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @Override
    public int countByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        return (int) repository.countByUser(userService.findOneById(userId));
    }

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        if (name == null || name.isEmpty()) throw new EmptyFieldException("Name");
        @NotNull Task task = new Task();
        task.setUser(userService.findOneById(userId));
        task.setName(name);
        return repository.save(task);
    }

    @NotNull
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        if (name == null || name.isEmpty()) throw new EmptyFieldException("Name");
        if (description == null || description.isEmpty()) throw new EmptyFieldException("Description");
        @NotNull Task task = new Task();
        task.setUser(userService.findOneById(userId));
        task.setName(name);
        task.setDescription(description);
        return repository.save(task);
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public boolean existsByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        if (id == null || id.isEmpty()) return false;
        return repository.existByUserIdAndId(userId, id);
    }


    @Override
    public @Nullable List<Task> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<Task> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        return repository.findByUser(userService.findOneById(userId));
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable String projectId
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();

        return repository.findByProject(projectService.findOneById(projectId));
    }

    @Nullable
    @Override
    public List<Task> findAllByUserIdAndProjectId(@Nullable final String userId, @Nullable String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findByUserAndProjectId(userService.findOneById(userId), projectId);
    }


    @Override
    public @Nullable Task findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Optional<Task> result = repository.findById(id);
        return result.orElse(null);
    }

    @Override
    public Task findOneByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<Task> result = repository.findByUserAndId(userService.findOneById(userId), id);
        return result.orElse(null);
    }

    @Override
    @Transactional
    public void remove(@Nullable Task model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException("Task");
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByUserId(@Nullable final String userId, @Nullable final Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        if (model == null) return;
        if (findOneByUserIdAndId(userId, model.getId()) == null) return;
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.deleteByUserAndId(userService.findOneById(userId), id);
    }

    @Nullable
    @Override
    @Transactional
    public Task update(@Nullable final Task model) throws Exception {
        if (model == null) return null;
        return repository.save(model);
    }

    @Nullable
    @Override
    @Transactional
    public Task updateByUserId(@Nullable final String userId, @Nullable final Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        if (model == null) return null;
        if (findOneByUserIdAndId(userId, model.getId()) == null) return null;
        model.setUser(userService.findOneById(userId));
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public Task updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyFieldException("Name");
        if (description == null || description.isEmpty()) throw new EmptyFieldException("Description");
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task updateByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyFieldException("Name");
        if (description == null || description.isEmpty()) throw new EmptyFieldException("Description");
        @Nullable final Task task = findOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @Override
    @Transactional
    public void updateProjectIdById(
            @Nullable final String id,
            @Nullable final String projectId
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(projectService.findOneById(projectId));
        repository.save(task);
    }

    @Override
    @Transactional
    public void updateProjectIdByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = findOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(projectService.findOneByUserIdAndId(userId, projectId));
        repository.save(task);
    }

}

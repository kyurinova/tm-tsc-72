package ru.tsc.kyurinova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.kyurinova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@EntityListeners(AuditingEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Project {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "row_id")
    @Id
    protected String id = UUID.randomUUID().toString();

    @NotNull
    public Project(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    public Project(@NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    @NotNull
    @Column
    private String name = "";

    @NotNull
    @Column(name = "descr")
    private String description = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "start_dt")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @Nullable
    @Column(name = "finish_dt")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finishDate;

    @Nullable
    @Column
    @CreatedDate
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created;

    @Column(name = "created_by")
    @NotNull
    @CreatedBy
    private String createdBy;

    @Column
    @NotNull
    @LastModifiedDate
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updated;

    @Column(name = "updated_by")
    @NotNull
    @LastModifiedBy
    private String updatedBy;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

}

package ru.tsc.kyurinova.tm.exeption.entity;

import ru.tsc.kyurinova.tm.exeption.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error. Entity not found.");
    }

}
